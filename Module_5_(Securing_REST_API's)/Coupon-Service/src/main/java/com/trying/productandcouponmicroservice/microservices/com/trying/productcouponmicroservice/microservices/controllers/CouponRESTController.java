package com.trying.productandcouponmicroservice.microservices.com.trying.productcouponmicroservice.microservices.controllers;


import com.trying.productandcouponmicroservice.microservices.com.trying.productcouponmicroservice.microservices.model.Coupon;
import com.trying.productandcouponmicroservice.microservices.com.trying.productcouponmicroservice.microservices.repos.CouponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/api/coupons")
public class CouponRESTController {

    @Autowired
    CouponRepository couponRepo;

    @PostMapping("/create")
    public Coupon createCoupon(@RequestBody Coupon coupon) {  // @RequestBody will covert incoming JSON body into class
        return couponRepo.save(coupon);
    }


    @GetMapping("/get/{code}")
    public Coupon getCoupon(@PathVariable("code") String code) {
        return couponRepo.findByCode(code);
    }
}
