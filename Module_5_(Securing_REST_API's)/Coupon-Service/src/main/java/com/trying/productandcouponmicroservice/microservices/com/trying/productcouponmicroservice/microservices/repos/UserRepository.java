package com.trying.productandcouponmicroservice.microservices.com.trying.productcouponmicroservice.microservices.repos;

import com.trying.productandcouponmicroservice.microservices.com.trying.productcouponmicroservice.microservices.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
        User findByEmail(String email);
}
