package com.trying.productandcouponmicroservice.microservices.com.trying.productcouponmicroservice.microservices.security.configuration;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class WebSecurityConfiguration {

    @Bean
    BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity security) throws Exception {
        security.httpBasic();
        security.csrf(csrf-> csrf.disable()) // if we not disable the post request is will not work
                .authorizeHttpRequests(auth-> {
                    auth.requestMatchers(HttpMethod.GET, "/v1/api/coupons/**")
                            .hasAnyRole("USER","ADMIN")
                            //for posting coupons
                            .requestMatchers(HttpMethod.POST,"/v1/api/coupons/**")
                            .hasRole("ADMIN"); //only admin can post or add new coupons
                });

        return security.build();
    }
}
