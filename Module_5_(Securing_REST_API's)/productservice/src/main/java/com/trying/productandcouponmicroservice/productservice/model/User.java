package com.trying.productandcouponmicroservice.productservice.model;


import jakarta.persistence.*;

import java.util.Set;

@Entity (name="users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // This is an identity field in database and it should be incremented
    private long id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;

    @JoinTable(name= "user_role", joinColumns = @JoinColumn(name="user_id"), inverseJoinColumns = @JoinColumn(name="role_id"))        //  We are using Join Table which we letting know to the Spring JPA
    @ManyToMany(fetch=FetchType.EAGER) // Normally in ManytoMany Relationship it Fetches roles Lazily, But here we want fast therefore FetchType.EAGER
    private Set<Role> roles; // One user Has more than one role also same role can assign more than one User.


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}
