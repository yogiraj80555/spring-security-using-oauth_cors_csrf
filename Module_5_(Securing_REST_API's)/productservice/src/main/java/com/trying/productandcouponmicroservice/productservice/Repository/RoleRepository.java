package com.trying.productandcouponmicroservice.productservice.Repository;

import com.trying.productandcouponmicroservice.productservice.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
    // Created JPA Repository For Role
}
