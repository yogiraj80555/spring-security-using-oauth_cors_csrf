package com.trying.productandcouponmicroservice.productservice.model;

import jakarta.persistence.*;
import org.springframework.security.core.GrantedAuthority;

import java.util.Set;

@Entity
public class Role implements GrantedAuthority {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // This is an identity field in database, and it should be incremented
    private long id;

    @Column(name = "name")
    private String roleName;

    @ManyToMany(mappedBy = "roles")  //this side relationship is already mapped by using 'inverse' in user class therefore add variable name 'roles' where we mapped.
    private Set<User> users;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    @Override
    public String getAuthority() {
        return roleName;
    }
}
