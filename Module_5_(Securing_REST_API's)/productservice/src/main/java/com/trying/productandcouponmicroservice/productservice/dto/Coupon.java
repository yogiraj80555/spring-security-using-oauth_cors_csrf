package com.trying.productandcouponmicroservice.productservice.dto;

import java.math.BigDecimal;

public class Coupon {

    private long id;
    private String code;
    private BigDecimal discount;
    private String expDate;


    // Setter
    public void setId(long id) {
        this.id = id;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }



    // Getter
    public long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public String getExpDate() {
        return expDate;
    }

}
