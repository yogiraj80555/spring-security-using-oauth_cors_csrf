package com.trying.productandcouponmicroservice.productservice.Repository;

import com.trying.productandcouponmicroservice.productservice.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
}
