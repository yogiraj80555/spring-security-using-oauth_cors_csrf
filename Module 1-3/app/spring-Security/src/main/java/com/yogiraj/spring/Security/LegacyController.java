package com.yogiraj.spring.Security;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
//here @controller always return view name
public class LegacyController {

    @RequestMapping(method = RequestMethod.GET, value = "/web")
    public String getData(){

        System.out.println("This is Database");
        return "data";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/page")
    @ResponseBody
    public String page() {
        return "Not-Returing-View-When-you-Used-@ResponseBody";
    }
}
