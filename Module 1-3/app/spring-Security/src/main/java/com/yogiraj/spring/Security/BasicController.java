package com.yogiraj.spring.Security;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController  //It is combination of @controller and @ResponseBody
public class BasicController {

    @GetMapping("/hello")
    public String Hello(){
        return "Spring Basic Security";
    }



}
