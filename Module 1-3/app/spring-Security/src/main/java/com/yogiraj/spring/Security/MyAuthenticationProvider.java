package com.yogiraj.spring.Security;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.Arrays;


@Component  // Otherwise it will not be Scanned
public class MyAuthenticationProvider implements AuthenticationProvider {
    @Override
    public Authentication authenticate(Authentication authentication /*It has username and password*/) throws AuthenticationException {
        // Authentication provider implementation
        String userName = authentication.getName();
        String password = authentication.getCredentials().toString();
        if("abcd".equalsIgnoreCase(userName) && "1234".equalsIgnoreCase(password))
        {
            // After this security Context store this in Authentication Success header
            return new UsernamePasswordAuthenticationToken(userName,password,/* List of Authorized roles*/ Arrays.asList());

        }
        //// After this security Context return this in Authentication failure header and gives 401 response to client.
        throw new BadCredentialsException("Wrong Credentials.");
    }

    @Override
    public boolean supports(Class<?> authentication) {
        // The Authentication Manager call this method after getting -> UsernamePasswordAuthenticationToken.
        // here we need to tell we support -> UsernamePasswordAuthenticationToken -> and this return boolean value.


        //return authentication.isInstance(UsernamePasswordAuthenticationToken.class);
                        /*   OR you can check this way also    */
        return authentication.equals(UsernamePasswordAuthenticationToken.class);

        // Mainly when AuthenticationManager invoked this method is passes UsernamePasswordAuthenticationToken into authenticate object
        // to check whether we support this or not.
    }
}
