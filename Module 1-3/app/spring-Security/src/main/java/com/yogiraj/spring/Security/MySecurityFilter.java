package com.yogiraj.spring.Security;

import jakarta.servlet.*;

import java.io.IOException;

public class MySecurityFilter implements Filter {


    // Note: Extends with oncePerrequestfilter.
    // This is an interface that you want to implement, if you want your filter logic to be executed only once when you create a filter.
    // There is no guarantee that your filter logic will be executed only once because spring can include your filter in the chain multiple times.
    // So it is very complex. If you go through the springs' filter chain, so if you want to guarantee that your filter logic should be execute
    // only once, you will extend this class. And the method in here is slightly different. If you hit control, one had and implemented methods.
    // So this the only difference is it gets the HTTP servlet request directly. It supports the HTTP protocol, no need of Servelet request.
    // And dealing with Servelet response, you get Servelet HTTP Servelet request and HTTP Servelet response directly.
    // That is the advantage here, although it says once per request filter it is our responsibility to implement the logic in such a way that it will be executed only once.
    // I just want to want you to know that these classes exist and at any point, if you want to use them, you can explore more by going to the documentation.

    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
            /*Compleate Chain of filters*/ FilterChain filterChain) throws IOException, ServletException {
        System.out.println("Before Own Chain Filter");
        System.out.println("here we decide that what we can do with current ServletRequest");

        filterChain.doFilter(servletRequest,servletResponse); //this will send request to next filter chain automatically

        System.out.println("After Own Chain Filter");
        System.out.println("here we decide that what we can do with current ServletResponse");

    }

}
