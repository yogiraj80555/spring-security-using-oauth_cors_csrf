package com.yogiraj.spring.Security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import java.net.http.HttpRequest;

@Configuration
public class MySecurityConfig {

//    @Bean
//    UserDetailsService userDetailsService(){
//        InMemoryUserDetailsManager inMemoryUserDetailsManager = new InMemoryUserDetailsManager();
//        UserDetails userDetails = User.withUsername("abcd")
//                .password(passwordEncoder().encode("1234"))
//                .authorities("read")
//                .build();
//        inMemoryUserDetailsManager.createUser(userDetails);
//        return inMemoryUserDetailsManager;
//    }

    @Bean
    BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    SecurityFilterChain filterRequest(HttpSecurity security) throws Exception {
        security.httpBasic(); //how request authorized
        //security.formLogin(); //Form based login  -> use browser
        //security.authorizeRequests().anyRequest().authenticated();  //want to authorized all request all some custom request to authorized

        security.authorizeRequests().requestMatchers("/hello").authenticated(); //Only '/hello' endpoint will require authentication.
        security.addFilterBefore(new MySecurityFilter(),
                /*Before which Filter you need to add your own Filter*/ BasicAuthenticationFilter.class);

       // security.authorizeRequests().requestMatchers("/hello").authenticated().anyRequest().denyAll(); // Only "\hello" endpoint will require authentication, but other endpoints not have access even successfully authentication Browser will show 403 Access denied
        return security.build();
    }
}
