package com.trying.productandcouponmicroservice.productservice.Controllers;


import com.trying.productandcouponmicroservice.productservice.Repository.ProductRepository;
import com.trying.productandcouponmicroservice.productservice.dto.Coupon;
import com.trying.productandcouponmicroservice.productservice.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.Optional;

@RestController
@RequestMapping("/v1/api/product")
public class ProductRestController {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    RestTemplate restTemplate;


    @Value("${coupon.service.url}")
    private String couponServiceApplicationUrl;

    @RequestMapping(value="/create", method = RequestMethod.POST)
    public Product create(@RequestBody Product product) {
        String couponCode = product.getCouponCode();

        //Coupon coupon = restTemplate.getForObject("http://127.0.0.1:8080/v1/api/coupons/get/"+couponCode, Coupon.class);
            /*
            *               This Can be write using property Injection like below
            * */
        Coupon coupon = restTemplate.getForObject(couponServiceApplicationUrl+couponCode, Coupon.class);



        BigDecimal productPrice = product.getPrice();
        BigDecimal discount = (coupon.getDiscount().divide(BigDecimal.valueOf(100.00))).multiply(productPrice);
        BigDecimal discountedPrice = productPrice.subtract(discount);
        System.out.println("Actual Product Price: "+productPrice);
        System.out.println("Discounted Price: "+discountedPrice);
        System.out.println("Discount: "+coupon.getDiscount()+"%");
        product.setPrice(discountedPrice);
        return productRepository.save(product);
    }




    @RequestMapping(value="/get/{id}", method = RequestMethod.GET)
    public Optional<Product> get(@PathVariable long id) {
        return productRepository.findById(id);
    }
}
