package com.trying.productandcouponmicroservice.microservices.com.trying.productcouponmicroservice.microservices.model;

import jakarta.persistence.*;
import org.springframework.security.core.GrantedAuthority;

import java.util.Set;

@Entity
public class Role implements GrantedAuthority {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // This is an identity field in database, and it should be incremented
    private long id;

    @Column(name = "name")
    private String roleName;

    @ManyToMany(mappedBy = "roles")  //this side relationship is allready mapped by using 'inverse' in user class therefore add variable name 'roles' where we mapped.
    private Set<User> users;



    @Override
    public String getAuthority() {   // It invoked internally by spring security to get Role of User and return name of Role
        return roleName;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
