package com.trying.productandcouponmicroservice.microservices.com.trying.productcouponmicroservice.microservices.security;

import com.trying.productandcouponmicroservice.microservices.com.trying.productcouponmicroservice.microservices.model.User;
import com.trying.productandcouponmicroservice.microservices.com.trying.productcouponmicroservice.microservices.repos.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl  implements UserDetailsService {


    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // Whatever the client sends in the request for authentication, that user name will be handed over to this load user by this user name
        // and we need to return a User details object from spring security that user details will be used by the authentication provider
        // within spring security and it will do the authentication for us.

        User user = userRepository.findByEmail(username);

        System.err.println("\n"+this.getClass()+"-> loadUserByUsername\n");

        // if user not found:- in case of wrong user Id
        if( user == null ){
            throw new UsernameNotFoundException("User Not Found for UserEmail: "+username);
        }

        return new org.springframework.security.core.userdetails.User(user.getEmail(),user.getPassword(),user.getRoles());
    }
}
