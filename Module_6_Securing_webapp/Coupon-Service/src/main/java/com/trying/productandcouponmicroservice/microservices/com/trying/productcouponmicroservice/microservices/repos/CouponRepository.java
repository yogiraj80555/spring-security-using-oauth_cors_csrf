package com.trying.productandcouponmicroservice.microservices.com.trying.productcouponmicroservice.microservices.repos;

import com.trying.productandcouponmicroservice.microservices.com.trying.productcouponmicroservice.microservices.model.Coupon;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CouponRepository extends JpaRepository<Coupon, Long> {
    Coupon findByCode(String code); //here automatically Spring JPA will create SQL query and select the row where this 'code' will match.

    // This is an use of JPA we need not write a SQL statement to retrieve or store data from/into database.
    // Internally Spring data will generate SQL query for use and get/store data on/from Database.
}
