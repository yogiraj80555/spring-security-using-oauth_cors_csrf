package com.trying.productandcouponmicroservice.microservices.com.trying.productcouponmicroservice.microservices.security;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public interface SecurityService {

    public boolean customLogin(String username, String password, HttpServletRequest request, HttpServletResponse response);
}
