package com.trying.productandcouponmicroservice.microservices.com.trying.productcouponmicroservice.microservices.controllers;


import com.trying.productandcouponmicroservice.microservices.com.trying.productcouponmicroservice.microservices.security.SecurityService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UserController {


    @Autowired
    SecurityService securityService;

    @GetMapping("/")
    public String showLoginPage() {
        return "login";
    }


    public String login(String email, String password /* this var name should be matched with login form fields or use @Requestparam to mapped form fields*/
            ,HttpServletRequest request, HttpServletResponse response) {
        boolean login = securityService.customLogin(email, password, request, response);
        if (login){
            return "index";
        }
        return "login";
    }
}
