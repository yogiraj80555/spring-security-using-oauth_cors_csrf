package com.trying.productandcouponmicroservice.microservices.com.trying.productcouponmicroservice.microservices.security.configuration;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.context.DelegatingSecurityContextRepository;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.security.web.context.RequestAttributeSecurityContextRepository;
import org.springframework.security.web.context.SecurityContextRepository;

@Configuration
public class WebSecurityConfiguration {

    @Bean
    BCryptPasswordEncoder passwordEncoder() {
        System.err.println("\n"+this.getClass()+" -> passwordEncoder\n");
        return new BCryptPasswordEncoder();
    }


    @Bean
    SecurityContextRepository securityContextRepository() {
        return new DelegatingSecurityContextRepository(new RequestAttributeSecurityContextRepository(),
                new HttpSessionSecurityContextRepository());
    }

    @Autowired
    UserDetailsService userDetailsService;    //don't remember how it gets Invoked

    @Bean
    AuthenticationManager authenticationManager() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setPasswordEncoder(passwordEncoder());
        provider.setUserDetailsService(userDetailsService);
        return new ProviderManager(provider);

    }

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity security) throws Exception {
        System.err.println("\n"+this.getClass()+" -> securityFilterChain\n");
        //security.httpBasic();
        security.formLogin();
        security.csrf(csrf-> csrf.disable()) // if we not disable the post request is will not work
                .authorizeHttpRequests(auth-> {
                    auth.requestMatchers(HttpMethod.GET, "/v1/api/coupons/**","/")
                            .hasAnyRole("USER","ADMIN")
                            //for posting coupons
                            .requestMatchers(HttpMethod.POST,"/v1/api/coupons/**","/saveCoupon")
                            .hasRole("ADMIN") //only admin can post or add new coupons

                            .requestMatchers(HttpMethod.GET,"/showCreateCoupon/**")
                            .hasRole("ADMIN")

                            .requestMatchers(HttpMethod.GET,"/showGetCoupon")
                            .hasAnyRole("USER","ADMIN")

                            .requestMatchers(HttpMethod.POST,"/getCoupon")
                            .hasAnyRole("USER","ADMIN");

                });


        // After Spring # we need to save security context manually for all http request to preserve session of users
        security.securityContext( context -> context.requireExplicitSave(true));

        return security.build();
    }
}
