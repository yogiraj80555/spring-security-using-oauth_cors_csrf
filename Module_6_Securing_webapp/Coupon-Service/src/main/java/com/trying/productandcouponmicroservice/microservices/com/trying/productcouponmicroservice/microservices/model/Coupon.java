package com.trying.productandcouponmicroservice.microservices.com.trying.productcouponmicroservice.microservices.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import java.math.BigDecimal;

@Entity   // This will make this class JPA Entity.
public class Coupon {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // This is an identity field in database and it should be incremented
    private long id;
    private String code;
    private BigDecimal discount;
    private String expDate;


    // Setter
    public void setId(long id) {
        this.id = id;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }



    // Getter
    public long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public String getExpDate() {
        return expDate;
    }
}
